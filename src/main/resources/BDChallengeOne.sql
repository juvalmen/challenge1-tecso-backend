CREATE SCHEMA IF NOT EXISTS `tecsobdtechnicaltest` DEFAULT CHARACTER SET utf8 ;
USE `tecsobdtechnicaltest` ;

CREATE TABLE categoria (
  `idcategoria` int(11) NOT NULL AUTO_INCREMENT,
  `llave` VARCHAR(30) NOT NULL,
  `nombre` VARCHAR(50) NOT NULL,
  `valor`  int(4) NOT NULL,
  PRIMARY KEY (`idcategoria`))
ENGINE = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE persona (
  `idpersona` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(80) NULL,
  `apellido` VARCHAR(250) NULL,
  `razonsocial` VARCHAR(100) NULL,
  `dni` VARCHAR(45) NULL,
  `cuit` VARCHAR(45) NOT NULL,
  `aniofundacion` INT NULL,
  `categoriapersona` INT NULL,
  PRIMARY KEY (`idpersona`),
  UNIQUE INDEX `cuit_UNIQUE` (`cuit` ASC),
  INDEX `fk_persona_idx` (`categoriapersona` ASC),
  CONSTRAINT `fk_persona_idcategoria_categoria`
    FOREIGN KEY (`categoriapersona`)
    REFERENCES `categoria` (`idcategoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE user (
  id int(11) NOT NULL AUTO_INCREMENT,
  username VARCHAR(45) NOT NULL,
  password VARCHAR(60) NOT NULL,
  email VARCHAR(60) NOT NULL,
  enabled SMALLINT NOT NULL,
  token VARCHAR(200) NULL,
  PRIMARY KEY (id))
  ENGINE = InnoDB DEFAULT CHARSET=utf8;
  