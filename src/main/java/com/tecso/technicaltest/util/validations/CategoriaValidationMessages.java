package com.tecso.technicaltest.util.validations;

/**
 * 
 * @author Julian Valencia
 * 18/05/2019
 */
public class CategoriaValidationMessages {
	
	// Categoria
	public static final String KEY_NO_ENCONTRADA = "No se ha encontrado la llave de la categoría";
}
