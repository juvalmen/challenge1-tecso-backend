package com.tecso.technicaltest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.tecso.technicaltest.config.AppBeanConfig;
import com.tecso.technicaltest.config.DatasourceConfig;
import com.tecso.technicaltest.config.SecurityConfig;
import com.tecso.technicaltest.config.swagger.Swagger2Config;

/**
 * 
 * @author Julian Valencia
 * 18/05/2019
 */
@SpringBootApplication
@Import(
	    value = {
	      AppBeanConfig.class,
	      DatasourceConfig.class,
	      SecurityConfig.class,
	      Swagger2Config.class
	    })	
public class MainApplication {

	public static void main(final String[] args) {
		SpringApplication.run(MainApplication.class, args);
	}
	
}
