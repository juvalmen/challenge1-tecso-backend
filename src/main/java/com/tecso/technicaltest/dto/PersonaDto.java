package com.tecso.technicaltest.dto;

import java.io.Serializable;

public class PersonaDto implements Serializable {

	private static final long serialVersionUID = 34343534604695006L;
	
	private Long idpersona;
	private String nombre;
	private String apellido;
	private String razonsocial;
	private String cuit;
	private String dni;
	private Integer aniofundacion;
	private Long idCategoriaPersona;
	private String valorCategoriaPersona;
    private String descripcionCategoriaPersona;
    
	public Long getIdpersona() {
		return idpersona;
	}
	public void setIdpersona(Long idpersona) {
		this.idpersona = idpersona;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getRazonsocial() {
		return razonsocial;
	}
	public void setRazonsocial(String razonsocial) {
		this.razonsocial = razonsocial;
	}
	public String getCuit() {
		return cuit;
	}
	public void setCuit(String cuit) {
		this.cuit = cuit;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public Integer getAniofundacion() {
		return aniofundacion;
	}
	public void setAniofundacion(Integer aniofundacion) {
		this.aniofundacion = aniofundacion;
	}
	public Long getIdCategoriaPersona() {
		return idCategoriaPersona;
	}
	public void setIdCategoriaPersona(Long idCategoriaPersona) {
		this.idCategoriaPersona = idCategoriaPersona;
	}
	public String getValorCategoriaPersona() {
		return valorCategoriaPersona;
	}
	public void setValorCategoriaPersona(String valorCategoriaPersona) {
		this.valorCategoriaPersona = valorCategoriaPersona;
	}
	public String getDescripcionCategoriaPersona() {
		return descripcionCategoriaPersona;
	}
	public void setDescripcionCategoriaPersona(String descripcionCategoriaPersona) {
		this.descripcionCategoriaPersona = descripcionCategoriaPersona;
	}
      
}