package com.tecso.technicaltest.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface UserLoginSecurityService extends UserDetailsService {

	public UserDetails loadUserByUsernameAndToken(final String username, String token) throws UsernameNotFoundException;
}