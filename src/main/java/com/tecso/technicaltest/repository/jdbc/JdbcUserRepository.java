package com.tecso.technicaltest.repository.jdbc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.tecso.technicaltest.model.User;

/**
 * 
 * @author Julian Valencia
 * 18/05/2019
 */
@Repository
public class JdbcUserRepository {

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public JdbcUserRepository(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	/**
	 * 
	 * @param key
	 * @return
	 * @throws EmptyResultDataAccessException
	 */
	public List<User> findUserByUsername(String username) throws EmptyResultDataAccessException {
		return jdbcTemplate.query("SELECT * FROM user where username = ?", 
				new Object[] { username },
				new BeanPropertyRowMapper<User>(User.class));
	}

}
