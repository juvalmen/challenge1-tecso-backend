package com.tecso.technicaltest.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tecso.technicaltest.model.Persona;

public interface JpaPersonaRepository extends JpaRepository<Persona, Long> {

}
