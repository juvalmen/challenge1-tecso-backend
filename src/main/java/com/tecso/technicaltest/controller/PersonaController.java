package com.tecso.technicaltest.controller;

import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tecso.technicaltest.dto.BaseResponseDto;
import com.tecso.technicaltest.dto.PersonaDto;
import com.tecso.technicaltest.service.PersonaService;
import com.tecso.technicaltest.util.SystemMessage;
import com.tecso.technicaltest.utils.exception.TecsoCustomException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@RestController
@RequestMapping("/persona")
@Api( value = "Servicio de administración de titulares para cuentas.", tags = { "Personas" })
public class PersonaController {
	
	private PersonaService  personaService;
	private static final Logger LOGGER = LogManager.getLogger(PersonaController.class.getName());
	
	public PersonaController(PersonaService personaService) {
		this.personaService = personaService;
	}	
	
	@ApiOperation(value = "Crea una persona.", response = BaseResponseDto.class,tags = { "Persona" })
	@ApiResponses(value = { 
	@ApiResponse(code = 200, message = "Persona creada.",response = BaseResponseDto.class),
	@ApiResponse(code = 400, message = "No se puede crear la persona. Excepción de validación", response = BaseResponseDto.class),
	@ApiResponse(code = 500, message = "Error interno en la aplicación.", response = BaseResponseDto.class), 
	})
	@PostMapping(value = "/save",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BaseResponseDto> savePerson(@RequestBody(required = true) PersonaDto personaDto) {
		BaseResponseDto response = new BaseResponseDto();
		HttpHeaders headers = new HttpHeaders();
		HttpStatus httpStatus = null;
		try {
			personaService.savePersona(personaDto);
			response.setResponseMessage(SystemMessage.STATUS_OK);
			httpStatus = HttpStatus.OK;
		} catch (TecsoCustomException e) {
			response.setStatusCode(HttpStatus.BAD_REQUEST.value());
			response.setResponseMessage(e.getLocalizedMessage());
			httpStatus = HttpStatus.BAD_REQUEST;
		} catch (Exception e) {
			response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setResponseMessage(SystemMessage.UNCONTROLED_ERROR);
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			LOGGER.error(e.getMessage());
		}
		response.setStatusCode(httpStatus.value());
		return new ResponseEntity<>(response, headers, httpStatus);
	}
	
	@ApiOperation(value = "Actualiza una persona.", response = BaseResponseDto.class,tags = { "Persona" })
	@ApiResponses(value = { 
	@ApiResponse(code = 200, message = "Persona actualizada.",response = BaseResponseDto.class),
	@ApiResponse(code = 400, message = "No se puede crear la persona. Excepción de validación", response = BaseResponseDto.class),
	@ApiResponse(code = 500, message = "Error interno en la aplicación.", response = BaseResponseDto.class), 
	})
	@PutMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BaseResponseDto> updatePerson(@RequestBody(required = true) PersonaDto personaDto) {
		BaseResponseDto response = new BaseResponseDto();
		HttpHeaders headers = new HttpHeaders();
		HttpStatus httpStatus = null;
		try {
			personaService.updatePersona(personaDto);
			response.setResponseMessage(SystemMessage.STATUS_OK);
			httpStatus = HttpStatus.OK;
		} catch (TecsoCustomException e) {
			response.setStatusCode(HttpStatus.BAD_REQUEST.value());
			response.setResponseMessage(e.getLocalizedMessage());
			httpStatus = HttpStatus.BAD_REQUEST;
		} catch (Exception e) {
			response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setResponseMessage(SystemMessage.UNCONTROLED_ERROR);
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			LOGGER.error(e.getMessage());
		}
		response.setStatusCode(httpStatus.value());
		return new ResponseEntity<>(response, headers, httpStatus);
	}
	
	@ApiOperation(value = "Consulta todas las personas.", response = BaseResponseDto.class,tags = { "Persona" })
	@ApiResponses(value = { 
	@ApiResponse(code = 200, message = "Consulta exitosa.",response = BaseResponseDto.class),
	@ApiResponse(code = 400, message = "Excepción de validación o consulta", response = BaseResponseDto.class),
	@ApiResponse(code = 500, message = "Error interno en la aplicación.", response = BaseResponseDto.class), 
	})
    @GetMapping(value = "/getAll", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BaseResponseDto> getAllPerson() {
		
		BaseResponseDto response = new BaseResponseDto();
		HttpHeaders headers = new HttpHeaders();
		HttpStatus httpStatus = null;
		try {
			List<PersonaDto> personas = personaService.getPersonasList();
			response.setResponseMessage(SystemMessage.STATUS_OK);
			response.setResponseBody(personas);
			httpStatus = HttpStatus.OK;
		} catch (TecsoCustomException e) {
			response.setStatusCode(HttpStatus.BAD_REQUEST.value());
			response.setResponseMessage(e.getLocalizedMessage());
			httpStatus = HttpStatus.BAD_REQUEST;
		} catch (Exception e) {
			response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setResponseMessage(SystemMessage.UNCONTROLED_ERROR);
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			LOGGER.error(e.getMessage());
		}
		response.setStatusCode(httpStatus.value());
		return new ResponseEntity<>(response,headers,httpStatus);
	}
	
	@ApiOperation(value = "Elimina una persona", response = BaseResponseDto.class,tags = { "Persona" })
	@ApiResponses(value = { 
	@ApiResponse(code = 200, message = "Registro eliminado.",response = BaseResponseDto.class),
	@ApiResponse(code = 400, message = "Excepción de validación o consulta", response = BaseResponseDto.class),
	@ApiResponse(code = 500, message = "Error interno en la aplicación.", response = BaseResponseDto.class), 
	})
	@DeleteMapping(value = "/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BaseResponseDto> deletePerson(@PathVariable long id) {
		BaseResponseDto response = new BaseResponseDto();
		HttpHeaders headers = new HttpHeaders();
		HttpStatus httpStatus = null;
		
		try {
			personaService.deletePersona(id);
			response.setResponseMessage(SystemMessage.STATUS_OK);
			httpStatus = HttpStatus.OK;
		} catch (TecsoCustomException e) {
			response.setStatusCode(HttpStatus.BAD_REQUEST.value());
			response.setResponseMessage(e.getLocalizedMessage());
			httpStatus = HttpStatus.BAD_REQUEST;
		} catch (Exception e) {
			response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setResponseMessage(SystemMessage.UNCONTROLED_ERROR);
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			LOGGER.error(e.getMessage());
		}
		response.setStatusCode(httpStatus.value());
		return new ResponseEntity<>(response,headers,httpStatus);
	} 

}
