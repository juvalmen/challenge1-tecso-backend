package com.tecso.technicaltest.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tecso.technicaltest.repository.jdbc.JdbcUserRepository;
import com.tecso.technicaltest.repository.jpa.JpaUserLoginRepository;
import com.tecso.technicaltest.service.UserLoginSecurityService;
import com.tecso.technicaltest.util.validations.SecurityValidationMessages;

@Service("userDetailsService")
public class UserLoginSecurityServiceImpl implements UserLoginSecurityService {

	JpaUserLoginRepository jpaUserRepository;
	JdbcUserRepository jdbcUserRepository;
	
	static final String ADMIN = "ADMIN";

	/**
	 * 
	 * @author Julian Valencia
	 * 20/05/2019
	 * @param jpaUserRepository
	 */
	@Autowired
	public UserLoginSecurityServiceImpl(JpaUserLoginRepository jpaUserRepository,
			JdbcUserRepository jdbcUserRepository) {
		this.jpaUserRepository = jpaUserRepository;
		this.jdbcUserRepository = jdbcUserRepository;
	}

	/**
	 * 
	 */
	@Transactional(readOnly = true)
	@Override
	public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {

		com.tecso.technicaltest.model.User user = jpaUserRepository.findUserByUsername(username);

		if (user != null) {
			List<GrantedAuthority> authorities = buildUserAuthority(ADMIN);
			User user2 = new User(user.getUsername(), user.getPassword(), user.isEnabled(), true, true, true, authorities);
			return user2;
		} else {
			throw new UsernameNotFoundException(SecurityValidationMessages.INVALID_TOKEN);
		}
	}

	/**
	 * 
	 */
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsernameAndToken(String username, String token)
			throws UsernameNotFoundException {

		List<com.tecso.technicaltest.model.User> users = jdbcUserRepository.findUserByUsername(username);
		com.tecso.technicaltest.model.User user = null;
		
		if(users != null && !users.isEmpty()) {
			user = users.get(0);
		}
		
		if (user != null && user.getToken().equals(token)) {
			List<GrantedAuthority> authorities = buildUserAuthority(ADMIN);
			User user2 = new User(user.getUsername(), user.getPassword(), user.isEnabled(), true, true, true, authorities);
			return user2;
		} else {
			throw new UsernameNotFoundException(SecurityValidationMessages.INVALID_TOKEN);
		}
	}
	

	/**
	 * 
	 * @param userRoles
	 * @return
	 */
	private List<GrantedAuthority> buildUserAuthority(String userRoles) {
		Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();
		setAuths.add(new SimpleGrantedAuthority(userRoles));

		return new ArrayList<>(setAuths);
	}
}
