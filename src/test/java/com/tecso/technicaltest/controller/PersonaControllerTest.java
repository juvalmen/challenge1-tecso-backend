package com.tecso.technicaltest.controller;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tecso.technicaltest.dto.PersonaDto;


@RunWith(SpringRunner.class)
@WebMvcTest(PersonaControllerTest.class)
public class PersonaControllerTest {
  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private ObjectMapper objectMapper;

	public PersonaDto getPersonaDto() {
		PersonaDto personaDto = new PersonaDto();
		personaDto.setNombre("Julian");
		personaDto.setApellido("Valencia");
		personaDto.setCuit("123456");
		personaDto.setDni("444444");
		personaDto.setRazonsocial("Arquitecto de Software");
		personaDto.setAniofundacion(1982);
		personaDto.setIdCategoriaPersona(1L);
		return personaDto;
	}

/*  
  @Test
  public void whenValidInput_thenReturnsStatus200() throws Exception {
	  
	   mockMvc.perform(post("/tecso/persona/getAll")
	        .contentType("application/json"))
	        .andExpect(status().isOk());
  }
  
  @Test
  public void whenValidInput_thenReturnsStatus400() throws Exception {
	  
	   mockMvc.perform(post("/tecso/persona/getAllAnyLink")
	        .contentType("application/json"))
	        .andExpect(status().isOk());
  }
*/
}
