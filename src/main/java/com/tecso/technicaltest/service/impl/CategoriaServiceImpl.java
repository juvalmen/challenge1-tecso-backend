package com.tecso.technicaltest.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tecso.technicaltest.dto.CategoriaDto;
import com.tecso.technicaltest.mappers.orika.DummyFieldMapper;
import com.tecso.technicaltest.model.Categoria;
import com.tecso.technicaltest.repository.jdbc.JdbcCategoriaRepository;
import com.tecso.technicaltest.repository.jpa.JpaCategoriaRepository;
import com.tecso.technicaltest.service.CategoriaService;
import com.tecso.technicaltest.util.validations.CategoriaValidationMessages;
import com.tecso.technicaltest.util.validations.PersonaValidationMessages;
import com.tecso.technicaltest.utils.exception.TecsoCustomException;

/**
 * 
 * @author Julian Valencia
 * 20/05/2019
 */
@Service
public class CategoriaServiceImpl implements CategoriaService {

	private JdbcCategoriaRepository jdbcCategoriaRepository;
	private JpaCategoriaRepository jpaCategoriaRepository;
	private DummyFieldMapper dummyFieldMapper;

	/**
	 * 
	 * @author Julian Valencia
	 * 20/05/2019
	 * @param jdbcTypeRepository
	 * @param jpaCategoriaRepository
	 * @param jdbcCategoriaRepository
	 * @param dummyFieldMapper
	 */
	@Autowired
	public CategoriaServiceImpl(JdbcCategoriaRepository jdbcTypeRepository, 
			JpaCategoriaRepository jpaCategoriaRepository,
			JdbcCategoriaRepository jdbcCategoriaRepository,
			DummyFieldMapper dummyFieldMapper) {
		this.jdbcCategoriaRepository = jdbcCategoriaRepository;		
		this.jpaCategoriaRepository = jpaCategoriaRepository;		
		this.dummyFieldMapper = dummyFieldMapper;		
	}

	/**
	 * 
	 */
	@Override
	public List<CategoriaDto> getCategoriaLlave(String key) throws TecsoCustomException {
		if (key == null || !key.isEmpty()) {
			throw new TecsoCustomException(CategoriaValidationMessages.KEY_NO_ENCONTRADA);
		}
		List<Categoria> list = jdbcCategoriaRepository.getCategoriaLlave(key);
		List<CategoriaDto> newList = new ArrayList<CategoriaDto>();
		list.forEach(categoria->{
			newList.add(dummyFieldMapper.map(categoria, CategoriaDto.class));
		});
		return newList;
	}

	/**
	 * 
	 */
	@Override
	public CategoriaDto getCategoriaPorId(Long id) throws TecsoCustomException {
		Categoria categoria = jpaCategoriaRepository.getOne(id);
		if(categoria.getIdcategoria() == null) {
		   throw new TecsoCustomException(PersonaValidationMessages.CATEGORIA_PERSONA_NO_EXISTE);
		}
		return dummyFieldMapper.map(categoria, CategoriaDto.class);
				
	}

}
