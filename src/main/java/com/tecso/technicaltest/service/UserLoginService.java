package com.tecso.technicaltest.service;

import com.tecso.technicaltest.dto.UserLoginDto;
import com.tecso.technicaltest.utils.exception.TecsoCustomException;

public interface UserLoginService {

	UserLoginDto getUserByUserName(String userName) throws TecsoCustomException;	
	UserLoginDto getUserByUserName(String userName,String token) throws TecsoCustomException;
	boolean setUserRegister(UserLoginDto userDto) throws TecsoCustomException;
	void setUserSave(UserLoginDto userDTO) throws TecsoCustomException;
	
}