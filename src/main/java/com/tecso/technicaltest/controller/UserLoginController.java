package com.tecso.technicaltest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tecso.technicaltest.config.securitytoken.JwtTokenUtil;
import com.tecso.technicaltest.dto.BaseResponseDto;
import com.tecso.technicaltest.dto.UserLoginDto;
import com.tecso.technicaltest.service.UserLoginService;
import com.tecso.technicaltest.util.SystemMessage;
import com.tecso.technicaltest.utils.exception.TecsoCustomException;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@RestController
@RequestMapping("/users")
public class UserLoginController {

	UserLoginService userService;
	private AuthenticationManager springAuthenticationManager;
	private UserDetailsService springUserDetailsService;
    private static final Logger LOGGER = LogManager.getLogger(UserLoginController.class.getName());

	public UserLoginController(UserLoginService userService,AuthenticationManager authenticationManager,UserDetailsService springUserDetailsService) {

		this.userService = userService;
		this.springAuthenticationManager = authenticationManager;
		this.springUserDetailsService = springUserDetailsService;
	}
	
	@ApiOperation(value = "Logueo de usuarios", response = BaseResponseDto.class,tags = { "Users" })
	@ApiResponses(value = { 
	@ApiResponse(code = 200, message = "Logueo exitoso.",response = BaseResponseDto.class),
	@ApiResponse(code = 400, message = "Excepción de validación o consulta.", response = BaseResponseDto.class),
	@ApiResponse(code = 401, message = "Usuario con credenciales incorrectas.", response = BaseResponseDto.class),
	@ApiResponse(code = 500, message = "Error interno en la aplicación.", response = BaseResponseDto.class), 
	})
	@RequestMapping(value = "/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BaseResponseDto> loginUser(@RequestBody UserLoginDto userDto) {
        
		String userName = userDto.getUsername();
        String password = userDto.getPassword();
 
		BaseResponseDto response = new BaseResponseDto();
		HttpHeaders headers = new HttpHeaders();
		HttpStatus httpStatus = HttpStatus.OK;
		try {
			
			UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(userName, password);
			Authentication authentication = this.springAuthenticationManager.authenticate(token);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			UserDetails userDetails = springUserDetailsService.loadUserByUsername(userName);

			List<String> roles = new ArrayList<>();

			for (GrantedAuthority authority : userDetails.getAuthorities()) {
				roles.add(authority.toString());
			}
			
			UserLoginDto user = userService.getUserByUserName(userName,
					JwtTokenUtil.generateToken(userDetails));
			
			response.setStatusCode(HttpStatus.OK.value());
			response.setResponseMessage(SystemMessage.STATUS_OK);
			response.setResponseBody(user);
		} catch (TecsoCustomException e) {
			response.setStatusCode(HttpStatus.BAD_REQUEST.value());
			response.setResponseMessage(e.getLocalizedMessage());
			httpStatus = HttpStatus.BAD_REQUEST;
		} catch(BadCredentialsException e){
			response.setStatusCode(HttpStatus.UNAUTHORIZED.value());
			response.setResponseMessage(SystemMessage.WRONG_USER_OR_PASSWORD);
			httpStatus = HttpStatus.UNAUTHORIZED;
		} catch (Exception e) {
			response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setResponseMessage(SystemMessage.UNCONTROLED_ERROR);
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			LOGGER.error(e.getMessage());
		}

		return new ResponseEntity<BaseResponseDto>(response, headers, httpStatus);
	}
	
	@ApiOperation(value = "Registro de usuarios", response = BaseResponseDto.class,tags = { "Users" })
	@ApiResponses(value = { 
	@ApiResponse(code = 200, message = "Registro exitoso.",response = BaseResponseDto.class),
	@ApiResponse(code = 400, message = "Excepción de validación o consulta.", response = BaseResponseDto.class),
	@ApiResponse(code = 500, message = "Error interno en la aplicación.", response = BaseResponseDto.class), 
	})
	@RequestMapping(value = "/register", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<BaseResponseDto> setRegisterUser(@RequestBody UserLoginDto userDto) {

		BaseResponseDto response = new BaseResponseDto();
		HttpHeaders headers = new HttpHeaders();
		HttpStatus httpStatus = HttpStatus.OK;

		try {
			response.setStatusCode(HttpStatus.OK.value());

			if (userDto != null && userDto.getUsername() != null && !userDto.getUsername().isEmpty()) {
				userService.setUserRegister(userDto);
				response.setResponseMessage(SystemMessage.STATUS_OK);
			} else {
				response.setResponseMessage(SystemMessage.EMPTY_USERNAME);
			}
		} catch (TecsoCustomException e) {
			response.setStatusCode(HttpStatus.BAD_REQUEST.value());
			response.setResponseMessage(e.getLocalizedMessage());
			httpStatus = HttpStatus.BAD_REQUEST;
		} catch (Exception e) {
			response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			response.setResponseMessage(SystemMessage.UNCONTROLED_ERROR);
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
			LOGGER.error(e.getMessage());
		}

		return new ResponseEntity<>(response, headers, httpStatus);
	}

}
