package com.tecso.technicaltest.repository.jdbc;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.tecso.technicaltest.dto.PersonaDto;

@Repository
public class JdbcPersonaRepository {

	private JdbcTemplate jdbcTemplate;
	EntityManager entityManager;

	@Autowired
	public JdbcPersonaRepository(JdbcTemplate jdbcTemplate,
			EntityManager entityManager) {
		this.jdbcTemplate = jdbcTemplate;
		this.entityManager = entityManager;
	}
		
	/**
	 * 
	 * @author Julian Valencia
	 * 18/05/2019
	 */
	public List<PersonaDto> getPersonaPorIdentificacionCuit(String cuit) {
		
		return jdbcTemplate.query("SELECT * from persona WHERE cuit = ?", 
				new Object[] { cuit },
				new BeanPropertyRowMapper<PersonaDto>(PersonaDto.class));
	}
	
	
	/**
	 * 
	 * @author Julian Valencia
	 * 18/05/2019
	 */
	public List<PersonaDto> getPersonasList() {
				
		StringBuilder sql = new StringBuilder();
		sql.append("  SELECT per.idpersona ")
		   .append(" ,per.nombre ")
		   .append(" ,per.apellido ")
		   .append(" ,per.dni ")
		   .append(" ,per.cuit ")
		   .append(" ,per.razonsocial ")
		   .append(" ,per.aniofundacion ")
		   .append(" ,cat.idcategoria as idCategoriaPersona ")
		   .append(" ,cat.valor as valorCategoriaPersona ")
		   .append(" ,cat.nombre as descripcionCategoriaPersona ")
		   .append(" FROM persona per ")
		   .append(" INNER JOIN categoria cat ON cat.idcategoria = per.categoriapersona; ");
		return jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper<PersonaDto>(PersonaDto.class));
	}
	
}
