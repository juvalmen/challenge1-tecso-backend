package com.tecso.technicaltest.service;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.tecso.technicaltest.dto.CategoriaDto;
import com.tecso.technicaltest.enums.CategoriaEnum;
import com.tecso.technicaltest.mappers.orika.DummyFieldMapper;
import com.tecso.technicaltest.model.Categoria;
import com.tecso.technicaltest.repository.jdbc.JdbcCategoriaRepository;
import com.tecso.technicaltest.repository.jpa.JpaCategoriaRepository;
import com.tecso.technicaltest.service.impl.CategoriaServiceImpl;
import com.tecso.technicaltest.utils.exception.TecsoCustomException;

@RunWith(SpringRunner.class)
@SpringBootTest()
@Transactional
@ActiveProfiles("test") 
public class CategoriaServiceImplTest {
	
	@Autowired
	@InjectMocks
	private CategoriaServiceImpl categoriaServiceImpl;

	@Mock
	private JdbcCategoriaRepository jdbcCategoriaRepository;
	
	@Mock
	private JpaCategoriaRepository jpaCategoriaRepository;
	
	@Mock
	private DummyFieldMapper dummyFieldMapper;
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * 
	 */
	public List<CategoriaDto> getListCategoriaDto() {
		List<CategoriaDto> list = new ArrayList<CategoriaDto>();
		list.add(getCategoriaDto());
		return list;
	}
	
	/**
	 * 
	 */
	public List<Categoria> getListCategoria() {
		List<Categoria> list = new ArrayList<Categoria>();
		list.add(getCategoria());
		return list;
	}
	
	/**
	 * 
	 */
	public CategoriaDto getCategoriaDto() {
		CategoriaDto categoriaDto = new CategoriaDto();
		categoriaDto.setIdcategoria(1L);
		categoriaDto.setNombre(CategoriaEnum.FISICA.getType());
		categoriaDto.setValor("1");
		categoriaDto.setLlave("categoriacategoria");
		return categoriaDto;
	}
	
	/**
	 * 
	 */
	public Categoria getCategoria() {
		Categoria categoria = new Categoria();
		categoria.setIdcategoria(1L);
		categoria.setNombre(CategoriaEnum.FISICA.getType());
		categoria.setValor("1");
		categoria.setLlave("categoriacategoria");
		return categoria;
	}
	
    @Test(expected = TecsoCustomException.class)
    public void test_categoryByKeyNull() throws TecsoCustomException {				
			categoriaServiceImpl.getCategoriaLlave(null);		
    }
    
    @Test
    public void test_categoryByKeyEmpty() throws TecsoCustomException {				
			categoriaServiceImpl.getCategoriaLlave("");		
    }
    
    @Test
    public void test_categoryByKey() throws TecsoCustomException {

			when(jdbcCategoriaRepository.getCategoriaLlave("")).thenReturn(getListCategoria());
			
			categoriaServiceImpl.getCategoriaLlave("");
	}
    
    @Test(expected = TecsoCustomException.class)
    public void test_categoryByIdNull() throws TecsoCustomException {
			Categoria categoria = getCategoria();
			categoria.setIdcategoria(null);
			CategoriaDto categoriaDto = getCategoriaDto();
			
			when(jpaCategoriaRepository.getOne(1L)).thenReturn(categoria);
			
			when(dummyFieldMapper.map(categoria, CategoriaDto.class)).thenReturn(categoriaDto);
			
			categoriaServiceImpl.getCategoriaPorId(1L);
	}  
    
    @Test
    public void test_categoryById() throws TecsoCustomException {
			Categoria categoria = getCategoria();
			CategoriaDto categoriaDto = getCategoriaDto();
			
			when(jpaCategoriaRepository.getOne(1L)).thenReturn(categoria);
			
			when(dummyFieldMapper.map(categoria, CategoriaDto.class)).thenReturn(categoriaDto);
			
			categoriaServiceImpl.getCategoriaPorId(1L);
	}  
   
}
