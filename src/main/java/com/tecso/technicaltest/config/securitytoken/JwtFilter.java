package com.tecso.technicaltest.config.securitytoken;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import com.tecso.technicaltest.service.UserLoginSecurityService;

@Configuration
public class JwtFilter extends GenericFilterBean {

	@Autowired
	private UserLoginSecurityService userLoginSecurityService;

	public JwtFilter(UserLoginSecurityService userLoginSecurityService) {
		this.userLoginSecurityService = userLoginSecurityService;
	}

	/**
	 * 
	 */
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {

		HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
		
		HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
		
		if(httpServletRequest.getMethod().equalsIgnoreCase("OPTIONS")) {
			filterChain.doFilter(servletRequest, servletResponse);
        } else {

        	if (!(((HttpServletRequest)servletRequest).getRequestURI().endsWith("/users/login") 
        			|| ((HttpServletRequest)servletRequest).getRequestURI().endsWith("/users/register") 
        			|| ((HttpServletRequest)servletRequest).getRequestURI().endsWith("/swagger-ui.html") 
        			|| ((HttpServletRequest)servletRequest).getRequestURI().endsWith("/webjars") 
        			|| ((HttpServletRequest)servletRequest).getRequestURI().endsWith("/error"))){
            	
            	String authToken = httpServletRequest.getHeader("Token");
            	
            	String actualUser = httpServletRequest.getHeader("User");
            	
            	if (StringUtils.isEmpty(authToken) || authToken.split(" ").length != 1) {
                    httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED,"Autentication error");
                } else {
	        		if (StringUtils.hasText(authToken)) {
	        			
	        			String username = actualUser;
	
	        			UserDetails userDetails = userLoginSecurityService.loadUserByUsernameAndToken(username, authToken);
	
	        			if (JwtTokenUtil.validateToken(authToken, userDetails)) {
	        				UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(userDetails,
	        						userDetails.getPassword(), userDetails.getAuthorities());
	        				SecurityContextHolder.getContext().setAuthentication(token);
	        			}
	        			filterChain.doFilter(servletRequest, servletResponse);
	        		}
                }
            } else {
            	filterChain.doFilter(servletRequest, servletResponse);
            }
        }

	}
}
