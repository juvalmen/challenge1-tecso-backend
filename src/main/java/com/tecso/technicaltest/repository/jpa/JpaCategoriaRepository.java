package com.tecso.technicaltest.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tecso.technicaltest.model.Categoria;

public interface JpaCategoriaRepository extends JpaRepository<Categoria, Long>{

}
